# learngit

官方网站 → https://www.liaoxuefeng.com

Git教程 → https://www.liaoxuefeng.com/wiki/896043488029600

此仓库用于测试Pull Request。

推送要求：

1. 按日期放入文件夹；

2. 必须使用英文文件名，文件名不能含有空格或特殊字符；

3. 文件扩展名必须为`.txt`或`.md`，不接受其他任何格式；

4. 文件编码必须是UTF-8，不接受其他任何编码；

5. 一次只提交1～2个文件。
