git reset <file> --hard HEAD~2   :回退2个版本
git restore <file> --staged :从暂存区中撤回修改    //  git reset HEAD <file>
git restore <file> :从工作区间撤回修改   //  git checkout -- file
git rm <file> :删除文件
git branch -d <branchName> :删除分支
git switch -c <branch> :代替 git checkout -b <branch>  新建并且切换到新的分支

merge --no-ff  能够将master分支与其他分支历史记录分开   更加清楚

git remote -v  :查看远程库的信息
如果在使用命令git remote add...时报错，这说明本地库已经关联了一个名叫origin的远程库，需要先删除这个origin远程库
git remote rm origin  ：删除已有的origin远程库
